# Foorge

## Installation

1. Copy the ip_addr.py into ``/usr/share/ansible_plugins/filter_plugins/ip_addr.py``
   This is code that will be in the next Ansible minor release (1.9) but isnt
   yet in a stable.

2. Run composer install


## Usage

Go to a project directory that has no ``Vagrantfile`` or ``deployment/``
directory yet.

Call::

    $ php foorge.php init <projectname>
    $ vagrant up
    $ php foorge.php service add mysql <projectname>
    $ vagrant provision

Available services:

- mysql
- couchdb
- beanstalk
